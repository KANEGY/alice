package org.example.coins_models;

import java.math.BigDecimal;

public class CoinModel {
    private String exchange;
    private String buy;
    private String sell;
    private BigDecimal price;
    private BigDecimal volume;

    public CoinModel(String buy, String sell, String exchange, BigDecimal price, BigDecimal volume) {
        this.buy = buy;
        this.sell = sell;
        this.exchange = exchange;
        this.price = price;
        this.volume = volume;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public String getBuy() {
        return buy;
    }

    public String getSell() {
        return sell;
    }


    @Override
    public String toString() {
        return
                "\"" + exchange + "\"" +
                        " " + buy +
                        "/" + sell +
                        " price: " + price
                ;
    }
}
