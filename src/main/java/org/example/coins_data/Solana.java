package org.example.coins_data;

import org.example.coins_models.CoinDataModel;

public class Solana extends CoinDataModel {
    public Solana() {
        cryptoId = "solana";
        totalPages = 3;
        tag = "SOL";
    }
}
