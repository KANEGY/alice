package org.example.Utility;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DataAndTime {
    public String getDataAndTime() {
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss dd.MM.yy");

        return simpleDateFormat.format(date);
    }

}
