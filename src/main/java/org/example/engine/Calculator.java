package org.example.engine;

import org.example.coins_models.CoinDataModel;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

public class Calculator {

    public ArrayList<String> findTrade(CoinDataModel coinDataModel) {
        ArrayList<String> valueTradeList = new ArrayList<>();
        for (int i = 0; i < coinDataModel.getBuyDataList().size(); i++) {
            for (int j = 0; j < coinDataModel.getSellDataList().size(); j++) {
                if (
                        isProfitable(coinDataModel.getBuyDataList().get(i).getPrice(), coinDataModel.getSellDataList().get(j).getPrice()) &&
                                coinDataModel.getBuyDataList().get(i).getSell().equals(coinDataModel.getSellDataList().get(j).getBuy()) &&
                                coinDataModel.getBuyDataList().get(i).getBuy().equals(coinDataModel.getSellDataList().get(j).getSell())) {

                    valueTradeList.add("Buy  " + coinDataModel.getBuyDataList().get(i) + "\nSell " +
                            coinDataModel.getSellDataList().get(j) + "\n");
                }
            }
        }
        System.out.println("that's all");
        return valueTradeList;
    }

    private static boolean isProfitable(BigDecimal buyPrice, BigDecimal sellPrice) {
        //   (1/sell) > buy
        BigDecimal one = new BigDecimal("1");
        BigDecimal inverseSellPrice = one.divide(sellPrice, RoundingMode.HALF_UP);
        return inverseSellPrice.compareTo(buyPrice) > 0;
    }
}
