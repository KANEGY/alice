package org.example.coins_data;
import org.example.coins_models.CoinDataModel;

public class Bnb extends CoinDataModel {
    public Bnb(){
        cryptoId = "binancecoin";
        totalPages = 6;
        tag = "BNB";
    }
}
