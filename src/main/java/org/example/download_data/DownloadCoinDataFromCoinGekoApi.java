package org.example.download_data;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.example.coins_models.CoinDataModel;
import org.example.coins_models.CoinModel;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class DownloadCoinDataFromCoinGekoApi {
    public void generateData(CoinDataModel coinDataModel) {
        try {
            String cryptoId = coinDataModel.getCryptoId();
            String apiUrl = "https://api.coingecko.com/api/v3/coins/" + cryptoId + "/tickers?page=";

            int currentPage = 1;
            int totalPages = coinDataModel.getTotalPages();
            List<JsonElement> allTickers = new ArrayList<>();

            while (currentPage <= totalPages) {
                URL url = new URL(apiUrl + currentPage);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setRequestMethod("GET");

                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));

                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                JsonElement jsonElement = JsonParser.parseString(response.toString());


                for (JsonElement ticker : jsonElement.getAsJsonObject().getAsJsonArray("tickers")) {
                    allTickers.add(ticker);
                }
                currentPage++;
            }
            for (JsonElement ticker : allTickers) {
                JsonObject tickerObject = ticker.getAsJsonObject();
                String exchangeName = tickerObject.get("market").getAsJsonObject().get("name").getAsString();
                String base = tickerObject.get("base").getAsString();
                String target = tickerObject.get("target").getAsString();
                BigDecimal currentPrice = tickerObject.get("last").getAsBigDecimal();
                BigDecimal volume = tickerObject.get("converted_volume").getAsJsonObject().get("usd").getAsBigDecimal();
                if (base.equals(coinDataModel.getTag())) {
                    coinDataModel.getBuyDataList().add(new CoinModel(base,target,exchangeName,currentPrice,volume));
                } else {
                    coinDataModel.getSellDataList().add(new CoinModel(base, target, exchangeName, currentPrice, volume));
                }
            }
        } catch (Exception e) {
            e.getMessage();
        }
    }
}
