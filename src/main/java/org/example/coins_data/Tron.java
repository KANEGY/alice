package org.example.coins_data;

import org.example.coins_models.CoinDataModel;

public class Tron extends CoinDataModel {
    public Tron(){
        cryptoId = "tron";
        totalPages = 3;
        tag = "trx";
    }
}
