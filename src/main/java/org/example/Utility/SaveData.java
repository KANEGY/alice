package org.example.Utility;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class SaveData {
    BufferedWriter writer = null;

    public void saveDataToFile(ArrayList<String> textData) throws IOException {
        DataAndTime dataAndTime = new DataAndTime();
        try {
            writer = new BufferedWriter(new FileWriter("trade.txt"));
            writer.write(dataAndTime.getDataAndTime() + "\n");
            for (String data : textData) {
                writer.write(data + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            writer.close();
        }
    }
}
