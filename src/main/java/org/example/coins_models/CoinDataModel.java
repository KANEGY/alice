package org.example.coins_models;

import java.util.ArrayList;

public abstract class CoinDataModel {
    protected String cryptoId;
    protected int totalPages;
    protected String tag;

    public CoinDataModel() {
        cryptoId = null;
        totalPages = 1;
        tag = null;
    }

    private static ArrayList<CoinModel> buyDataList = new ArrayList<>();
    private static ArrayList<CoinModel> sellDataList = new ArrayList<>();

    public ArrayList<CoinModel> getBuyDataList() {
        return buyDataList;
    }

    public ArrayList<CoinModel> getSellDataList() {
        return sellDataList;
    }

    public String getCryptoId() {
        return cryptoId;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public String getTag() {
        return tag;
    }
}
