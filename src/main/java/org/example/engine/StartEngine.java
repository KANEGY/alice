package org.example.engine;

import org.example.Utility.SaveData;
import org.example.coins_models.CoinDataModel;
import org.example.download_data.DownloadCoinDataFromCoinGekoApi;

import java.io.IOException;

public class StartEngine {
    public StartEngine(CoinDataModel coinDataModel) throws IOException {
        engineOrder(coinDataModel);
    }

    private void engineOrder(CoinDataModel coinDataModel) throws IOException {
        DownloadCoinDataFromCoinGekoApi downloadCoinDataFromApi = new DownloadCoinDataFromCoinGekoApi();
        Calculator calculator = new Calculator();
        SaveData saveData = new SaveData();

        downloadCoinDataFromApi.generateData(coinDataModel);
        saveData.saveDataToFile(calculator.findTrade(coinDataModel));
    }


}
