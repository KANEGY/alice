package org.example.coins_data;

import org.example.coins_models.CoinDataModel;


public class Matic extends CoinDataModel {
    public Matic(){
        cryptoId = "matic-network";
        totalPages = 4;
        tag = "MATIC";
    }
}
